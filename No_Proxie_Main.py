import requests
from urllib.parse import quote
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import re
import random
import pandas as pd
import sqlite3


USER_AGENTS = [
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36"
]

def get_random_user_agent():
    return random.choice(USER_AGENTS)

def check_cache_indexed(url):
    # Путь к драйверу
    driver_path = 'C:\\Users\\zimar\\Desktop\\GoogleScript\\driver'

    # Получаем рандомный User-agent
    options = webdriver.ChromeOptions()
    options.add_argument(f'user-agent={get_random_user_agent()}')
    driver = webdriver.Chrome(executable_path=driver_path, options=options)


    search_query = f'site:{url}'
    url = f'https://www.google.com/search?q={quote(search_query)}'


    driver.get(url)
    time.sleep(5)  # Ждём полной загрузки.

    # Есть ли кнопка с кукис?
    try:
        cookies_button = driver.find_element(By.ID, 'L2AGLb')
        if cookies_button:
            cookies_button.click()
            time.sleep(3)
    except:
        pass

    # Получае количество результатов
    result_stats = driver.find_element(By.CSS_SELECTOR, '#result-stats').text


    matches = re.search(r'Результатов: примерно (\d+)', result_stats)
    if matches:
        result_count = int(matches.group(1))
        if result_count >= 1:
            print('URL проиндексирован')
        else:
            print('URL не проиндексирован')
    else:
        print('Не удалось получить количество результатов')

    # Обращаемся к кэшу
    cache_search_query = f'cache:{url}'
    cache_url = f'https://www.google.com/search?q={quote(cache_search_query)}'

   
    driver.get(cache_url)

    time.sleep(5)

    # Проверяем, есть ли кэш?
    cache_header = driver.find_elements(By.ID, 'bN015htcoyT__google-cache-hdr')
    if cache_header:
        print('URL имеется в кэше Google')
    else:
        print('URL нет в кэше Google')

    # Заурываем окно
    driver.close()

    # Закрываем сессию
    driver.quit()

    # Сохранение результатов в базу данных
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    # Проверка, проиндексирован ли URL
    if result_count >= 1:
        google_url_index = 'URL проиндексирован'
    else:
        google_url_index = 'URL не проиндексирован'

    # Проверка наличия URL в кэше Google
    if cache_header:
        google_url_cache = 'URL имеется в кэше Google'
    else:
        google_url_cache = 'URL нет в кэше Google'

    # Вставка данных в таблицу базы данных
    cursor.execute("INSERT INTO urls (URL, GoogleURLIndex, GoogleURLCache) VALUES (?, ?, ?)",
                   (url, google_url_index, google_url_cache))

    # Сохранение изменений и закрытие соединения
    conn.commit()
    conn.close()

def check_url_cache(url):
    headers = {'User-Agent': get_random_user_agent()}
    try:
        response = requests.get(url, headers=headers, verify=False)
        if response.status_code == 200:
            check_cache_indexed(url)
        else:
            print(f'URL {url} недоступен')
    except requests.exceptions.SSLError as e:
        print(f'Ошибка SSL при отправке запроса: {e}')
    
    # Сохранение результатов в базу данных
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    if response.status_code == 200:
        pass
    else:
        print(f'URL {url} недоступен')
        # Если URL недоступен, сохраняем соответствующие данные в базу данных
        cursor.execute("INSERT INTO urls (URL, GoogleURLIndex, GoogleURLCache) VALUES (?, ?, ?)",
                       (url, 'URL недоступен', 'URL недоступен'))
        conn.commit()

    conn.close()

# Создание базы данных и таблицы, если они не существуют
conn = sqlite3.connect('database.db')
cursor = conn.cursor()

cursor.execute('''CREATE TABLE IF NOT EXISTS urls
                  (URL TEXT, GoogleURLIndex TEXT, GoogleURLCache TEXT)''')

conn.commit()
conn.close()

# Чтение CSV-файла 
df = pd.read_csv('1.csv')
urls = df['source_url'].tolist()


for url in urls:
    if url != 'https://nomadicsoft.io/ru/':
        check_url_cache(url)
        time.sleep(30)  # Ожидание 30 секунд между запросами