import sqlite3

# Установка соединения с базой данных SQLite
conn = sqlite3.connect('database.db')

# Создание курсора для выполнения операций с базой данных
cursor = conn.cursor()

# Создание таблицы
cursor.execute('''CREATE TABLE IF NOT EXISTS urls
                  (URL TEXT, GoogleURLIndex TEXT, GoogleURLCache TEXT)''')

# Сохранение изменений и закрытие соединения
conn.commit()
conn.close()
